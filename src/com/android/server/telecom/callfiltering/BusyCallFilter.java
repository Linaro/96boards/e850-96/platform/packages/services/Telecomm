/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.android.server.telecom.callfiltering;

import android.content.Context;
import android.net.Uri;
import android.telecom.DisconnectCause;
import android.telecom.Log;
import android.telecom.VideoProfile;
import android.telephony.CarrierConfigManager;

import com.android.server.telecom.Call;
import com.android.server.telecom.CallsManager;
import com.android.server.telecom.CarrierConfigUtil;

public class BusyCallFilter implements IncomingCallFilter.CallFilter {

    private Context mContext;
    private CallsManager mCm;

    public BusyCallFilter(Context context, CallsManager cm) {
        mContext = context;
        mCm = cm;
    }

    @Override
    public void startFilterLookup(final Call call, CallFilterResultCallback callback) {
        boolean isWaitingCall = mCm.hasActiveOrHoldingCall();
        boolean hasVideoCall = mCm.hasVideoCall();
        boolean isVideoCall = (call.getVideoState() == VideoProfile.STATE_TX_ENABLED) ||
            (call.getVideoState() == VideoProfile.STATE_RX_ENABLED) ||
            (call.getVideoState() == VideoProfile.STATE_BIDIRECTIONAL);

        Log.d(this, "isWaitingCall=" + isWaitingCall + ", hasVideoCall=" + hasVideoCall +
                ", isVideoCall=" + isVideoCall);

        if (CarrierConfigUtil.getBooleanCarrierConfig(mContext,
                CarrierConfigManager.KEY_RESTRICT_WAITING_VT_CALL_NW, mCm.getSubId(call))
            && isWaitingCall && (hasVideoCall || isVideoCall)) {
            call.setDisconnectCause(new DisconnectCause(DisconnectCause.BUSY));
            callback.onCallFilteringComplete(call, new CallFilteringResult(false, true,
                        true, true));
        } else {
            callback.onCallFilteringComplete(call, new CallFilteringResult(true, false, true, true));
        }

    }
}
